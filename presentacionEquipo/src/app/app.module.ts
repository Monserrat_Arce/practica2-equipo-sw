import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { C1Component } from './component/c1/c1.component';
import { C2Component } from './component/c2/c2.component';
import { C3Component } from './component/c3/c3.component';
import { C4Component } from './component/c4/c4.component';
import { C5Component } from './component/c5/c5.component';
import { C6Component } from './component/c6/c6.component';
import { Compo1Component } from './components1/compo1/compo1.component';
import { Compo2Component } from './components1/compo2/compo2.component';
import { Compo3Component } from './components1/compo3/compo3.component';
import { Compo4Component } from './components1/compo4/compo4.component';
import { Componente2Component } from './Componente2/componente2/componente2.component';
import { C7Component } from './component/c7/c7.component';
import { C8Component } from './component/c8/c8.component';
import { C9Component } from './component/c9/c9.component';
import { C10Component } from './component/c10/c10.component';

@NgModule({
  declarations: [
    AppComponent,
    C1Component,
    C2Component,
    C3Component,
    C4Component,
    C5Component,
    C6Component,
    Compo1Component,
    Compo2Component,
    Compo3Component,
    Compo4Component,
    Componente2Component,
    C7Component,
    C8Component,
    C9Component,
    C10Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
